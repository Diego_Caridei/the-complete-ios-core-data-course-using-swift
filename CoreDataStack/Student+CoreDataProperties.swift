//
//  Student+CoreDataProperties.swift
//  CoreDataStack
//
//  Created by Diego Caridei on 29/04/16.
//  Copyright © 2016 Fahir Mehovic. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Student {

    @NSManaged var name: String?

}
