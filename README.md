# The complete iOS Core Data course using Swift
Are you struggling to learn how to make data driven apps for iOS and Cocoa? Do you want to learn how can you implement Core Data in your project but you don't know where to start? Then this is the course for you.

In this course you will not learn basics of Core Data, you will learn all of the advanced features of Core Data. Core Data is a framework that apple uses for data driven iOS and Cocoa applications.

If you are an apple developer then it is essential for you to know how to implement this framework in your applications, and that's why we will not only explore how to implement core data when we check that " Use Core Data " check box, we will build our own Core Data Stack and that way get to know the Core of Core Data.

We will start with a simple example of what Table Views are and how can we use them, then we will take a look at why is Core Data important in iOS programming. After that we will build our own Core Data Stack from scratch, we will get to know all Core Data components and how to use them in order to build our Core Data Stack, this will help you implement Core Data in projects where you did not check " Use Core Data " check box. Then we will learn to edit and delete items in core data, after that we will take a look at advanced fetching, so we will so how can we can a specific item in our data by using the items name or number, we will even sort our data in ascending and descending order using letters and numbers, and we can use advanced fetching to delete a specific item that we want. At the end we will see how can we combine TableViews with Core Data using NSFetchedResultsController.

You will also benefit from my super fast response if you have any issue that you are stuck with(I check Udemy forums every day if someone posts a question). Oh and all the students taking the course will also be there to help you!

All project files will be included and you are free to use them for anything that you like, personal or commercial use!

What This Course DOES NOT Cover

Importing Data From Other Files(Plists etc) Into Core Data
Connecting Core Data On iCloud


What This Course Covers

Modeling
Fetching
Editing
Deleting
Displaying data in user interface
Sorting
Advanced Fetching
Asynchronous Fetching
Syncing table views with Core Data
Migration
If you already know how to make basic iOS apps, join this course to become a professional data driven app developer.

Enroll Now!! You Will Not Be Disappointed!!

What are the requirements?

Xcode
What am I going to get from this course?

Over 30 lectures and 2 hours of content!
Write data driven iOS and Cocoa Applications
Model data in the model editor
Create a Core Data Stack from ground up
Edit and Delete in Core Data
Create relationships between entities
Sort fetched data
Search for a specific item in Core Data
Use asynchronous fetching to not interrupt the main thread
Sync Table views with Core Data
Migrate Core Data
What is the target audience?

This is not an entry level course, I expect you to know your way around xcode and swift. I expect you to know about delegation,table views, key/value coding, target/action, you dont need to be an expert in those things but you need to be able to look those things up when you need them. However, I dont expect you to know anything about core data. So if you're looking to upgrade your skills in apple development and learn how to write data driven applications you are in the right place
