//
//  Course+CoreDataProperties.swift
//  Relationships
//
//  Created by Diego Caridei on 01/05/16.
//  Copyright © 2016 Fahir Mehovic. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Course {

    @NSManaged var title: String?
    @NSManaged var instructor: Instructor?

}
