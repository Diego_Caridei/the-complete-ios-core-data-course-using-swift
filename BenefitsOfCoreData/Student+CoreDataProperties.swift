//
//  Student+CoreDataProperties.swift
//  BenefitsOfCoreData
//
//  Created by Diego Caridei on 27/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Student {

    @NSManaged var name: String?

}
