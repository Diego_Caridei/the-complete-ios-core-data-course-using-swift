//
//  ViewController.swift
//  BenefintsOfCoreData
//
//  Created by Fahir Mehovic on 5/27/15.
//  Copyright (c) 2015 Fahir Mehovic. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var myTableView: UITableView!
    
    var context: NSManagedObjectContext!;
    
    var results : NSFetchedResultsController!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "Students";
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell");
        results.delegate = self
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        myTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return results.sections!.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.sections![section].numberOfObjects;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return results.sections![section].name
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath);
        cell.textLabel?.text = results.objectAtIndexPath(indexPath).name
        return cell;
    }
    
    func loadData() {
        let request = NSFetchRequest(entityName: "Student")
        let sort = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sort]
        results = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: "lastName", cacheName: "lastName")
        try! results.performFetch()
        
        
    }
    
    func saveStudent(name : String) {
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate;
        let context = delegate.managedObjectContext;
        
        let student = NSEntityDescription.insertNewObjectForEntityForName("Student", inManagedObjectContext: context!);
        
        student.setValue(name, forKey: "name");
        
        do {
        
            try context?.save()
            
        } catch {
            print("problem");
        }
        
        
        
        
    }

    @IBAction func addNewStudent(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Add Student", message: "Add New Student", preferredStyle: .Alert);
        
        let save = UIAlertAction(title: "Save", style: .Default, handler: { (alertAction: UIAlertAction) -> Void in
           
            let textField = alert.textFields![0] as UITextField;
            self.saveStudent(textField.text!);
            self.myTableView.reloadData();
            
        });
        
        let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: { (alertAction: UIAlertAction) -> Void in
        });
        
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField) -> Void in
        }
        
        alert.addAction(save);
        alert.addAction(cancel);
        
        presentViewController(alert, animated: true, completion: nil);
        
    }

}





























