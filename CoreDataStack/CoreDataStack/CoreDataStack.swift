//  CoreDataStack
//
//  Created by Diego Caridei on 29/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//
import Foundation
import CoreData

class CoreDataStack {
    
    let context: NSManagedObjectContext
    let psc: NSPersistentStoreCoordinator
    let model : NSManagedObjectModel
    
    
    
    init (){
        
        let bundle = NSBundle.mainBundle()
        let modelURL = bundle.URLForResource("Data", withExtension: "momd")
        model = NSManagedObjectModel(contentsOfURL: modelURL!)!
        psc = NSPersistentStoreCoordinator(managedObjectModel: model)
        context = NSManagedObjectContext()
        context.persistentStoreCoordinator =  psc
        
        let appDir = appliactionDocumentsDirectory()
        let storeURL = appDir.URLByAppendingPathComponent("Data")
        let option = [NSMigratePersistentStoresAutomaticallyOption:true]
        
        var err:NSError?
        
        do{
            try psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: option)
            
        }catch let err1 as NSError{
            err = err1
        }
        if (err != nil){
            print("Could not create store")
            abort()
        }
    }
    
    func save(){
        var err:NSError?
        
        do{
            try context.save()
            
        }catch let err1 as NSError{
            err = err1
        }
        if (err != nil){
            print("Could not save data")
            abort()
        }
    }
    
    func appliactionDocumentsDirectory() -> NSURL {
        let fileManager = NSFileManager.defaultManager()
        let urls = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[0]
    }
}


































