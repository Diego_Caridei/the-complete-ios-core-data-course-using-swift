//
//  ViewController.swift
//  TableViewDemo
//
//  Created by Diego Caridei on 26/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    let data = ["Data 1","Data 2","Data 3","Data 4","Data 5"]
    let data1 = ["New Data 1","New Data 2","New Data 3","New Data 4","New Data 5"]
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section==0) {
            return data.count
        }
        else{
            return data1.count
        }
    }
    
    
    
    
    
    //Title for section (TableView)
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section==0 {
            return "Old Data"
        }else{
            return "New Data"
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath)
        
        if ( indexPath.section == 0){
            cell.textLabel?.text = data[indexPath.row];
        }
        else{
            cell.textLabel?.text = data1[indexPath.row];

        }
        cell.detailTextLabel?.text = "Diego Caridei"
        
        return cell
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

