//
//  ViewController.swift
//  BenefitsOfCoreData
//
//  Created by Diego Caridei on 26/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource {

    var students: [Student] = []
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Students"
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        //Reference to delegate
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = delegate.managedObjectContext
        
        let request = NSFetchRequest(entityName: "Student")
        
        var error:NSError?
        do{
            students = try context.executeFetchRequest(request) as! [Student]
            
        }catch let err1 as NSError{
            error = err1
        }
        if(error != nil){
            print("Problem with loading Data")
        }

    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell",forIndexPath: indexPath)
        cell.textLabel?.text = students[indexPath.row].name

        return cell
    }
    
    func saveStudent(name:String){
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = delegate.managedObjectContext
        let student = NSEntityDescription.insertNewObjectForEntityForName("Student", inManagedObjectContext: context) as! Student
        student.setValue(name, forKey: "name")
        var error:NSError?
        do{
            try context.save()
            self.students.append(student)
        }catch let err1 as NSError{
            error = err1
        }
        if(error != nil){
            print("Problem with saving Data")
        }
    }
    
    @IBAction func addNewStudent(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Add Student", message: "Add new Student", preferredStyle: .Alert)
        
        let save = UIAlertAction (title: "Save", style: .Default) { (action: UIAlertAction) in
            let textField = alert.textFields![0] as UITextField
            self.saveStudent(textField.text!)
            self.myTableView.reloadData()
        }
        
        let cancel = UIAlertAction (title: "Cancel", style: .Default) { (action: UIAlertAction) in
     
        }
        //Add textfield to alert
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField) in
            
        }
        
        alert.addAction(save)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

