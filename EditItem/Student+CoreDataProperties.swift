//
//  Student+CoreDataProperties.swift
//  CoreDataStack
//
//  Created by Diego Caridei on 29/04/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//
//  Delete this file and regenerate it using "Create NSManagedObject Subclass…"
//  to keep your implementation up to date with your model.
//

import Foundation
import CoreData

extension Student {

    @NSManaged var name: String?

}
