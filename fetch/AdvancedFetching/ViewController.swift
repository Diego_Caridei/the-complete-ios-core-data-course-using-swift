//
//  ViewController.swift
//  AdvancedFetching
//
//  Created by Diego Caridei


import UIKit
import CoreData

class ViewController: UIViewController {
    
    var students: [Student] = [];
    var context: NSManagedObjectContext!;
    var model : NSManagedObjectModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveStudent(name: String, age: Int) {
        let student = NSEntityDescription.insertNewObjectForEntityForName("Student", inManagedObjectContext: context) as! Student;
        
        student.name = name;
        student.age = age;
        
        try! context.save()
        
        students.append(student);
    }
    
    func search(name: String) {
        /*
        // ==[c] not case cansitive
        //name BEINGSWITH[c]
        //name LIKE[c](format: "name LIKE[c] '\(name)*'") we can use just 1,2,..3 letter
        //format: "num > 10"
        let predicate = NSPredicate(format: "name ==[c] '\(name)'")
        
        //The name is the key (the same on the datamodel)
        //let sort = NSSortDescriptor (key: "name", ascending: true)
        let request = NSFetchRequest(entityName: "Student")
        //request.sortDescriptors = [sort]
        request.predicate = predicate
        */
        
       // let request = model.fetchRequestTemplateForName("name")!

        
        /*
        let udpate = NSBatchUpdateRequest(entityName: "Student")
        udpate.propertiesToUpdate = ["name": "Universal Name"]
        udpate.affectedStores  = context.persistentStoreCoordinator?.persistentStores
        udpate.resultType = NSBatchUpdateRequestResultType.UpdatedObjectsCountResultType
        
        try! context.executeRequest(udpate)
        */
        
        let request = NSFetchRequest(entityName: "Student")
        
        let async = NSAsynchronousFetchRequest(fetchRequest: request){ (result: NSAsynchronousFetchResult) -> Void in
            self.students = result.finalResult as! [Student]
            
            for student in self.students {
                print("Student Name: \(student.name), Student Age: \(student.age)\n")
            }
        }
        
         try! context.executeRequest(async) 
    
        for student in students {
            print("Student Name: \(student.name), Student Age: \(student.age)\n")
        }
        
    }
    
    
    @IBAction func addStudent(sender: AnyObject) {
        
        let alert = UIAlertController(title: "New Student", message: "Add student name and age", preferredStyle: .Alert);
        
        let search = UIAlertAction(title: "Save", style: .Default) { (alertAction: UIAlertAction) -> Void in
            let textField = alert.textFields![0] ;
            
            let textFieldAge = alert.textFields![1] ;
            
            if let age = Int(textFieldAge.text!) {
                self.saveStudent(textField.text!, age: age);
            }
            
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .Default) { (alertAction: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
        }
        
        alert.addAction(search);
        alert.addAction(cancel);
        
        presentViewController(alert, animated: true, completion: nil);
        
    }
    
    @IBAction func searchStudent(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Search For Student", message: "Enter Student Name", preferredStyle: .Alert);
        
        let save = UIAlertAction(title: "Search", style: .Default) { (alertAction: UIAlertAction) -> Void in
            let textFIeld = alert.textFields![0]
            self.search(textFIeld.text!);
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .Default) { (alertAction: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
        }
        
        alert.addAction(save);
        alert.addAction(cancel);
        
        presentViewController(alert, animated: true, completion: nil);
        
    }


} // class














































