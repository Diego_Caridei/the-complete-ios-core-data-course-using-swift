//
//  Student+CoreDataProperties.swift
//  AdvancedFetching
//  Created by Diego Caridei


import Foundation
import CoreData

extension Student {

    @NSManaged var age: NSNumber?
    @NSManaged var name: String?

}
